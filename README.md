# Veyo Front End Challenge

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.14.0.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

To deal with the JSON file manipulations, I created a node server that can be run locally. 

You can fork and clone the server at: https://github.com/mdeggies/veyo-server, and then run `npm start` to start the server with nodemon. 

## Testing

Running `grunt test` will run the unit tests with karma.