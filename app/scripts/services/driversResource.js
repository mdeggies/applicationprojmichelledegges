'use strict';

/**
* @ngdoc service
* @name frontEndChallengeApp.Drivers
* @description Interacts with the NodeJS server, hosted locally, to handle all JSON file manipulations
* # Drivers
* Factory in the frontEndChallengeApp.
*/
angular.module('frontEndChallengeApp')
.factory('Drivers', function($resource) {
  return $resource('http://localhost:5000/api/data/:id', {id: '@id'});
});
