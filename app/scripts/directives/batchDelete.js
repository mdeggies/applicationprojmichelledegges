'use strict';

/**
* @ngdoc directive
* @name frontEndChallengeApp.directive:batchDelete
* @description Directive to delete multiple drivers
* # batchDelete
*/

var app = angular.module('frontEndChallengeApp');

app.directive('batchDelete', function(Drivers) {
  return {
    template: '<button class="btn btn-danger">BATCH DELETE</button>',
    restrict: 'E',
    scope: false,
    link: function postLink(scope, element, attrs) {
      element.on('click', function() {
        var drivers = scope.data;
        var checkboxes = scope.checkboxes.items;
        var toDelete = [];
        // Delete drivers with checked checkboxes
        angular.forEach(checkboxes, function(value, id) {
          if (value) {
            toDelete.push(id);
          }
        });
        for (var i=toDelete.length-1; i>=0; i--) {
          var id = toDelete[i];
          Drivers.remove({id: id});
          scope.data.splice(id-1, 1);
        }
        // Update ID's in JSON file
        for (var i=0; i<scope.data.length; i++) {
          scope.data[i].id = i+1;
        }
        Drivers.save(scope.data);
      });
    }
  }
});
