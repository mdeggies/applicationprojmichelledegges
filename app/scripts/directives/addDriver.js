'use strict';

/**
* @ngdoc directive
* @name frontEndChallengeApp.directive:addDriver
* @description Directive to add a new driver
* # addDriver
*/

var app = angular.module('frontEndChallengeApp');

app.directive('addDriver', function(Drivers) {
  return {
    template: '<button class="btn btn-primary submit" ng-disabled="addDriver.$invalid" ng-model="driverData">ADD DRIVER</button>',
    restrict: 'E',
    scope: false,
    link: function postLink(scope, element, attrs) {
      element.on('click', function(e) {
        var form = scope.addDriver;
        var driverData = scope.driverData;
        var lat = parseFloat(driverData.lat);
        var long = parseFloat(driverData.long);
        var complete = 0;
        // Validate form values
        if ((driverData.name === "") || (driverData.name.length <= 3)) {
          $('#name').val("Enter a valid driver's name").css({color: 'red'});
        } else { complete++; $('#name').css({color: 'black'}); }
        if ((driverData.device != "Driver Device") && (driverData.device != "Company Issued Device")) {
          $('#device').val("Enter 'Driver Device' or 'Company Issued Device'").css({color: 'red'});
        } else { complete++; $('#device').css({color: 'black'}); }
        if ((driverData.status != 'Active') && (driverData.status != 'Inactive') && (driverData.status != 'Suspended')) {
          $('#status').val("Enter 'Active' or 'Inactive' or 'Suspended'").css({color: 'red'});
        } else { complete++; $('#status').css({color: 'black'}); }
        if ((isNaN(lat)) || (lat < -90) || (lat > 90)) {
          $('#lat').val("Enter a number in the range -90 to 90").css({color: 'red'});
        } else { complete++; $('#lat').css({color: 'black'}); }
        if ((isNaN(long)) || (long < -180) || (lat > 180)) {
          $('#long').val("Enter a number in the range -180 to 180").css({color: 'red'});
        } else { complete++; $('#long').css({color: 'black'}); }
        // Once form validation is complete
        // Add user to table & JSON, reset form
        if (complete === 5) {
          driverData.id = scope.data.length+1;
          scope.data.push(driverData);
          Drivers.save(scope.data, function(res) {
            scope.driverData = null;
            form.$setPristine();
          });
        }
      });
    }
  }
});
