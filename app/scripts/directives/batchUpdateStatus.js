'use strict';

/**
* @ngdoc directive
* @name frontEndChallengeApp.directive:batchUpdateStatus
* @description Directive to update multiple driver statuses
* # batchUpdateStatus
*/

var app = angular.module('frontEndChallengeApp');

app.directive('batchUpdateStatus', function(Drivers) {
  return {
    template: '<button class="btn btn-primary" ng-model="updatedStatus">BATCH UPDATE STATUS</button>',
    restrict: 'E',
    scope: false,
    link: function postLink(scope, element, attrs) {
      element.on('click', function() {
        var drivers = scope.data;
        var input = scope.updatedStatus.toLowerCase();
        var checkboxes = scope.checkboxes.items;
        var updated = false;
        // Change drivers status when input is valid
        if ((input === "active") || (input === "inactive") || (input === "suspended")) {
          angular.forEach(checkboxes, function(value, id) {
            if (value) {
              if (drivers[id-1].status != input) {
                updated = true;
                drivers[id-1].status = input.charAt(0).toUpperCase() + input.slice(1);;
              }
            }
          });
          // Update drivers in table & JSON
          if (updated) {
            Drivers.save(drivers, function(resp) {
              scope.data = drivers;
            });
          }
        }
      });
    }
  }
});
