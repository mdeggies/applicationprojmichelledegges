'use strict';

/**
* @ngdoc function
* @name frontEndChallengeApp.controller:MainCtrl
* @description
* # MainCtrl
* Controller of the frontEndChallengeApp
*/
angular.module('frontEndChallengeApp').controller('MainCtrl', MainCtrl);
MainCtrl.$inject = ['$scope', '$filter', 'NgTableParams', 'Drivers'];
function MainCtrl($scope, $filter, NgTableParams, Drivers) {

  $scope.tableParams = new NgTableParams({
    page: 1,
    count: 10
  }, {
    getData: function($defer, params) {
      Drivers.query(function(data){
        $scope.data = data;
        params.total(data.length);

        var orderedData = params.sorting() ?
        $filter('orderBy')($scope.data, params.orderBy()) :
        $scope.data;
        orderedData = params.filter() ?
        $filter('filter')(orderedData, params.filter()) :
        orderedData;
        $scope.orderedData = orderedData;

        params.total(orderedData.length);
        $defer.resolve($scope.drivers = orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
      });
    }
  });

  // Initialize map
  $scope.map = {
    center: {
      latitude: 32.7673865,
      longitude: -117.1583822
    },
    zoom: 12,
    bounds: {}
  };

  // When new drivers are added or deleted, update map markers
  $scope.$watchCollection('data', function(drivers) {
    $scope.markers = [];
    angular.forEach(drivers, function(driver) {
      if (driver.status === 'Active') {
        var icon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
      }
      if (driver.status === 'Inactive') {
        var icon = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
      }
      if (driver.status === 'Suspended') {
        var icon = 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png';
      }
      var driver = {
        id: driver.id,
        latitude: driver.lat,
        longitude: driver.long,
        title: driver.name,
        icon: icon
      };
      $scope.markers.push(driver);
    });
  });

  // Show driver details on map marker click
  $scope.driverDetails = function(marker, eventName, model) {
    model.show = !model.show;
    marker.setAnimation(google.maps.Animation.BOUNCE);
    setTimeout(function () {
        marker.setAnimation(null);
    }, 750);
  };

  // Sort table by category: 'Name', 'Device', 'Status'
  $scope.sort = function(value) {
    value = value.toLowerCase();
    if ((value === 'name') || (value === 'device') ||(value === 'status')) {
      $scope.sortOrder = value;
    }
  };

  var inArray = Array.prototype.indexOf ?
  function (val, arr) {
    return arr.indexOf(val)
  } :
  function (val, arr) {
    var i = arr.length;
    while (i--) {
      if (arr[i] === val) return i;
    }
    return -1
  };

  $scope.checkboxes = { 'checked': false, items: {} };

  $scope.$watch('checkboxes.checked', function(value) {
    angular.forEach($scope.orderedData, function(item) {
      if (angular.isDefined(item.id)) {
        $scope.checkboxes.items[item.id] = value;
      }
    });
  });

  $scope.$watch('checkboxes.items', function(values) {
    if (!$scope.drivers) {
      return;
    }
    var checked = 0, unchecked = 0,
    total = $scope.drivers.length;
    angular.forEach($scope.drivers, function(item) {
      checked   +=  ($scope.checkboxes.items[item.id]) || 0;
      unchecked += (!$scope.checkboxes.items[item.id]) || 0;
    });
    if ((unchecked == 0) || (checked == 0)) {
      $scope.checkboxes.checked = (checked == total);
    }
    angular.element(document.getElementById("select_all")).prop("indeterminate", (checked != 0 && unchecked != 0));
  }, true);
}
