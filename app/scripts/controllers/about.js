'use strict';

/**
 * @ngdoc function
 * @name frontEndChallengeApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the frontEndChallengeApp
 */
angular.module('frontEndChallengeApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
