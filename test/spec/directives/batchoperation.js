'use strict';

describe('Directive: batchOperation', function () {

  // load the directive's module
  beforeEach(module('frontEndChallengeApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<batch-operation></batch-operation>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the batchOperation directive');
  }));
});
